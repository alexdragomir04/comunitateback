// express,mysql,axios,body-parser

const express = require("express");
const bodyParser = require("body-parser");
const mysql = require('mysql2');

//crearea variabilei de tip Express si parsarea in JSON

const app = express();
app.use(bodyParser.json());

//legarea pe front

app.use("/", express.static("../front-end"));

//deschiderea serverului pe portul 8080

app.listen(8080, 'localhost', () => {
    console.log("Server started on 8080");
});

//crearea conexiunii la baza de date
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "Masinutze"
})

// conectarea la BD si crearea unui table

connection.connect((err) => {
    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS Masini(marca VARCHAR(25),cp INTEGER,culoare VARCHAR(15),capacitateMotor INTEGER)";
    connection.query(sql, (err) => {
        if (err) throw err;
    })
})

// crearea unui obiect de tip masina pentru inserarea in baza de date printr-un post

app.post("/masina", (req, res) => {
    const masina = {
        marca: req.body.marca,
        cp: req.body.cp,
        culoare: req.body.culoare,
        capacitateMotor: req.body.capacitateMotor
    }
    let errors = []

    // validari pe cele 4 campuri

    // inserarea in baza de date a obiectului

    if (errors.length == 0) {
        const inserare = "INSERT INTO Masini(marca,cp,culoare,capacitateMotor) VALUES ('" + masina.marca + "','" + masina.cp + "','" + masina.culoare + "','" + masina.capacitateMotor + "')";
        connection.query(inserare, (err) => {
            if (err) throw err;
            else {
                console.log("Masina inserata!");
                res.status(200).send({
                    message: "Ati achizitionat o masina!",
                    masina


                })
            }
        })
    } else {
        console.log("O eroare s-a produs!");
        res.status(500).send(errors);
    }

});